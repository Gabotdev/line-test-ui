import FormConfirmPayment from "@/components/FormConfirmPayment"

const ConfirmarPayment = () => {
  return (
    <div className="w-full min-h-screen pt-12 bg-slate-300">
        <div className="w-96 mx-auto bg-slate-100 min-h-fit p-5">
          <FormConfirmPayment/>
        </div>
    </div>  
    )
}

export default ConfirmarPayment