import FormCreatePayment from "../../components/FormCreatePayment"

export const metadata = {
  title: 'Create Payment',
}

const CreatePayment = async ({children}) => {
  return (
    <div className="w-full min-h-screen pt-12 bg-slate-300">
        <div className="w-96 mx-auto bg-slate-100 min-h-fit p-5">
            <FormCreatePayment/>
        </div>
    </div>
)
}

export default CreatePayment