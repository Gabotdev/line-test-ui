import axios from "axios";

import httpClient from "./httpClient";

function handleError(axiosError, result) {
	const message = axiosError.response?.data?.Mensaje;
    console.log(message)

	result.message = 'Ocurrio un error inesperado. Por favor vuelva a intentar más tarde.';


	return result;
}

class LineAPICore {
    async createPayment (form) {
		const result = {
			data: null,
			message: '',
			status: false,
		};
		try {
			const { data } = await httpClient.post({
				url: `/payments`, body: form
			});
                if (data.CodigoError === 0) {
                    result.status = true;
                }
				result.data = data;

			return result;
		} catch (error) {
			console.log('> error in payments', error);

			return handleError(error, result);
		}
	}
    

    // async createSplitPayment () {

    // }

    async anulatePayment (id) {
        const result = {
			data: null,
			message: '',
			status: false,
		};
		try {
			const { data } = await httpClient.post({
				url: `/payments/anulacion/${id}`
			});
                if (data.CodigoError === 0) {
                    result.status = true;
                }
				result.data = data;

			return result;
		} catch (error) {
			console.log('> error in anulatePayment', error);

			return handleError(error, result);
		}

    }

    async verifyAmount (form) {
        const result = {
			data: null,
			message: '',
			status: false,
		};
		try {
			const { data } = await httpClient.post({
				url: `/payments/verificar-compra`, body: form
			});
                if (data.Validado) {
                    result.status = true;
                }
				result.data = data;

			return result;
		} catch (error) {
			console.log('> error in confirm payment', error);

			return handleError(error, result);
		}

    }

    async verifyCodigo (compraID) {
        const result = {
			data: null,
			message: '',
			status: false,
		};
		try {
            // this is hardcoded
            // este código no se usará en producción
            // es solo para pruebas
			const { data } = await httpClient.get({
				url: `/payments/test-code/${compraID}`
            });
                if (!data?.Message) {
                    result.status = true;
                }
				result.data = data;

			return result;
		} catch (error) {
			console.log('> error in verifyCodigo', error);

			return handleError(error, result);
		}
    }
}

export const LINEAPI = new LineAPICore();
