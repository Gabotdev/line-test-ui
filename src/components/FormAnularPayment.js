"use client";
import { useRouter } from "next/navigation";
import { toast } from "react-toastify";

import { LINEAPI } from "@/app/lib/LIneAPI";

const FormAnularPayment = () => {
    const router = useRouter();
    const codigoConfirm = localStorage.getItem('CodigoAnulacion');

  return (
    <form
        onSubmit={async (e) => {
            e.preventDefault();
            const result = await LINEAPI.anulatePayment(codigoConfirm);
            if (result.status) {
                toast.success('Anulación exitosa')
                localStorage.setItem('CodigoAnulacion', '')
                return router.push('/')
            }
    
            toast.error('Ocurrió un error en la anulación del pago')
        }}
    >
        <input type="submit" value="Anular transacción" 
            className="cursor-pointer border-solid border-black border-2 p-2"
        />
    </form>
  )
}

export default FormAnularPayment