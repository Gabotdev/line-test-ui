import axios from "axios";
import { URL_API } from "../constants";

const URL_BASE = `${URL_API}`;

const readUrl = (url) => {
  return url.startsWith("http://") || url.startsWith("https://")
    ? url
    : `${URL_BASE}${url}`;
};

// Authorization Token
// const getToken = () => {
//   return {
//     token: sessionStorage.getItem(LOCAL_STORAGE_KEYS.accessToken),
//   };
// };

const HEADERS_DEFAULT = {
  Accept: "application/json",
  "Content-Type": "application/json",
  "Access-Control-Allow-Origin": "*",
};

const get = ({ url = "", options = {}, headers = {} }) => {

  return axios.get(readUrl(url), {
    headers: {
      ...HEADERS_DEFAULT,
      ...headers,
    },
    ...options,
  });
};

const post = ({ url = "", body = {}, headers = {}, options = {} }) => {

  const { headers: headers_, ...restOptions } = options;

  return axios.post(readUrl(url), body, {
    headers: {
      ...HEADERS_DEFAULT,
      ...headers_,
    },
    ...restOptions,
  });
};

const put = ({ url = "", body = {}, headers = {}, options = {} }) => {

  return axios.put(readUrl(url), body, {
    headers: {
      ...HEADERS_DEFAULT,
      ...headers,
    },
    ...options,
  });
};

const _delete = ({ url = "", body = {}, headers = {}, options = {} }) => {

  return axios.delete(readUrl(url), {
    headers: {
      ...HEADERS_DEFAULT,
      ...headers,
    },
    ...options,
  });
};

/* trunk-ignore(eslint/import/no-anonymous-default-export) */
export default {
  get,
  post,
  put,
  delete: _delete,
};
