"use client";
import { useState } from "react";
import { useRouter } from "next/navigation";

import randomID from "../app/helpers/createID";
import { LINEAPI } from "../app/lib/LIneAPI";
import { toast } from "react-toastify";

const FormCreatePayment = () => {
    const router =   useRouter();

    const [formPayment, setFormPayment] = useState({
        IdentificadorCliente: "",
        CanalVenta: "WEB",
        TerminalTipo: "VIRTUAL",
        TerminalSistema: "TERM001",
        ModoIngreso: "WEB",
        Moneda: "ARS",  //check
        NumeroTarjeta: "4111111111111111", // 16 digitos check
        FechaExpiracion: "2312", //check
        CodigoSeguridad: "123",// check
        CodigoEmisor: "", //Tarjeta VISA, MASTERCARD, AMEX. check
        TarjetaTipo: "CREDITO", // check
        TipoDocumento: "DNI", // check
        DocumentoTitular: "34453456", //check
        NombreTitular: "Dana Golden", // check
        EmailTitular: "dana.golden@gmail.com", //check
        Referencia: "SKU 7766353", // check
        IdentificadorComprador: "34453456",
        CompraVerificada: {
            ModoVerificacion: "INMEDIATO",
            UnidadTiempo: "HORA",
            CantidadTiempo: 48
        },
        Detalle: [
            {
                NumeroComercio: "24719411",
                Importe: 3500,
                Cuotas: 1,
                IdentificadorCliente: "",
                Referencia: "SKU 7766353",
                NombreFantasia: "allaccess.com/rhcp"
            }  ,
            {
                NumeroComercio: "24719411",
                Importe: 9482,
                Cuotas: 2,
                IdentificadorCliente: "",
                Referencia: "SKU 7766353",
                NombreFantasia: "allaccess.com/rhcp"
            }
        ]
    })

    const handleChange = (e) => {
        const { name, value } = e.target;
        if (name == "Referencia") {
            return setFormPayment((prevState) => {
                const Detalle = prevState.Detalle.map((item) => {
                    item[name] = value
                    return item;
                })
                return {
                    ...prevState, Detalle, [name]: value
                };
            })
        }
        setFormPayment((prevState) => ({
            ...prevState, [name]: value
        }))
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        const id = randomID();

        const form = {...formPayment};
        form.IdentificadorCliente = id;
        form.Detalle = form.Detalle.map((item) => {
            item.IdentificadorCliente = id;
            return item;
        })

        const result = await LINEAPI.createPayment(form);
        if (result.status) {
            toast.success('Success')
            localStorage.setItem('CodigoAnulacion', id);
            localStorage.setItem("CodigoConfirm", result.data.CompraVerificada.CompraVerificadaID)
            return router.push('/confirmar-payment')
        }

        toast.error("Ocurrio un error")
    }
    return (
    <form onSubmit={handleSubmit} method="POST">

        <div>
            <label htmlFor="NombreTitular" className="block">Nombre titular</label>
            <input className="p-2" name="NombreTitular" type="text" id="NombreTitular" onChange={handleChange} value={formPayment.NombreTitular} />
        </div>

        <div>
            <label htmlFor="EmailTitular" className="block">Email titular</label>
            <input className="p-2" name="EmailTitular" type="email" id="EmailTitular" onChange={handleChange} value={formPayment.EmailTitular} />
        </div>

        <div>
            <label htmlFor="TipoDocumento" className="block">Tipo de documento</label>
            <select className="p-2" id="TipoDocumento" name="TipoDocumento" onChange={handleChange} value={formPayment.TipoDocumento}>
                <option value=""></option>
                <option value="DNI">DNI</option>
                <option value="Pasaporte">Pasaporte</option>
                <option value="LibretaEnrolamiento">Libreta Enrolamiento</option>
                <option value="LibretaCivica">Libreta Civica</option>
            </select>
        </div>
        <div>
            <label htmlFor="DocumentoTitular" className="block">Número del documento</label>
            <input className="p-2" name="DocumentoTitular" type="text" id="DocumentoTitular" onChange={handleChange} value={formPayment.DocumentoTitular} />
        </div>

        <div>
            <label htmlFor="NumeroTarjeta" className="block">Numero de Tarjeta</label>
            <input className="p-2" name="NumeroTarjeta" type="text" id="NumeroTarjeta" onChange={handleChange} value={formPayment.NumeroTarjeta} />
        </div>
        <div>
            <label htmlFor="FechaExpiracion" className="block">Fecha de Expiracion</label>
            <input className="p-2" name="FechaExpiracion" type="text"  id="FechaExpiracion" onChange={handleChange} value={formPayment.FechaExpiracion} />
        </div>
        <div>
            <label htmlFor="CodigoSeguridad" className="block">Código de Seguridad</label>
            <input className="p-2" name="CodigoSeguridad" type="text"  id="CodigoSeguridad" onChange={handleChange}value={formPayment.CodigoSeguridad} />
        </div>

        <div>
            <label htmlFor="CodigoEmisor" className="block">Tarjeta emisora</label>
            <select className="p-2" id="CodigoEmisor" name="CodigoEmisor" onChange={handleChange} value={formPayment.CodigoEmisor}>
                <option value=""></option>
                <option value="VISA">VISA</option>
                <option value="MASTER">MASTER</option>
                <option value="AMEX">AMEX</option>
            </select>
        </div>

        <div>
            <label htmlFor="TarjetaTipo" className="block">Tipo de tarjeta</label>
            <select className="p-2" id="TarjetaTipo" name="TarjetaTipo" onChange={handleChange} value={formPayment.TarjetaTipo}>
                <option value=""></option>
                <option value="CREDITO">CREDITO</option>
                <option value="DEBITO">DEBITO</option>
            </select>
        </div>

        <div>
            <label htmlFor="Moneda" className="block">Moneda</label>
            <select className="p-2" id="Moneda" name="Moneda" onChange={handleChange} value={formPayment.Moneda}>
                <option value=""></option>
                <option value="ARS">Peso argentino</option>
                <option value="U$S">Dolares</option>
            </select>
        </div>

        <div>
            <label htmlFor="Referencia" className="block">Referencia. Introduce el mismo nombre si es un pago recurrent</label>
            <input className="p-2" name="Referencia" type="text"  id="Referencia" onChange={handleChange}value={formPayment.Referencia} />
        </div>

        <input type="submit" value="Realizar pago" className="cursor-pointer border-solid border-black border-2 p-2"/>
    </form>
  )
}

export default FormCreatePayment