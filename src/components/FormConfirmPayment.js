"use client";
import { useState, useEffect } from "react";
import { toast } from "react-toastify"

import { LINEAPI } from "@/app/lib/LIneAPI";
import { useRouter } from "next/navigation";

const FormConfirmPayment = () => {
    const router = useRouter();
    const codigoConfirm = localStorage.getItem('CodigoConfirm');
    if (codigoConfirm == "") {
        router.push('/');
    }

    
    const [confirmPayment, setConfirmPayment] = useState({
        Codigo: "",
        CompraVerificadaID: codigoConfirm,
        CantidadMaximaIntentos: 5,
        CantidadTiempoExtensionVencimiento: 1,
        UnidadTiempoExtensionVencimiento: "HORA",
        IPAddress: "182.22.33.11"
    })
    
    useEffect(() => {
        async function getCode() {
            const result = await LINEAPI.verifyCodigo(codigoConfirm);
            console.log(result)
            setConfirmPayment((prevState) => ({
                ...prevState, Codigo: result.data
            }));
        }
        getCode()
    }, [])

    const handleSubmit = async (e) => {
        e.preventDefault();

        const result = await LINEAPI.verifyAmount(confirmPayment);

        if (result.status) {
            toast.success('Confirmación exitosa')
            localStorage.setItem('CodigoConfirm', '')
            return router.push('/')
        }

        toast.error('Ocurrió un error en la confirmación');
    }

  return (
    <form onSubmit={handleSubmit} method="POST">
        <div>
            <label className="block" htmlFor="Codigo">Código de confirmación</label>
            <input type="text" className="p-2" id="Codigo" name="Codigo" value={confirmPayment.Codigo}
                onChange={(e) => {
                    const {value, name} = e.target;
                    setConfirmPayment((prevState) => ({
                        ...prevState, [name]: value
                    }))
                }}
            />
        </div>
        <input type="submit" value="Confirmar"
        className="cursor-pointer border-solid border-black border-2 p-2"
        />
    </form>
  )
}

export default FormConfirmPayment